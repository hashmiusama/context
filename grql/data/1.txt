input graph=
nodes: [
    {
        "id": "us",
        "label": "us",
        "properties": {}
    },
    {
        "id": "usb",
        "label": "usb",
        "properties": {}
    },
    {
        "id": "cde",
        "label": "cde",
        "properties": {}
    },
    {
        "id": "usc",
        "label": "usc",
        "properties": {
            "age": 32
        }
    },
    {
        "id": "uscd",
        "label": "uscd",
        "properties": {
            "age": 31
        }
    }
]
edges:[
    {
        "id": "ad",
        "label": "ad",
        "from": "us",
        "to": "usb",
        "properties": {}
    },
    {
        "id": "bc",
        "label": "bc",
        "from": "usb",
        "to": "cde",
        "properties": {}
    },
    {
        "id": "aga",
        "label": "aga",
        "from": "usb",
        "to": "usc",
        "properties": {
            "count": 200
        }
    },
    {
        "id": "aga2",
        "label": "aga2",
        "from": "usb",
        "to": "uscd",
        "properties": {
            "count": 200
        }
    }
]

query:
select [a]-/b/-[c]-/d/-[e] from graph where [$a="label",@a="us"],[$b="label",@b="ad"],[$c="label",@c="usb"],[$d="label",@d="bc"],[$e="label",@e="cde"];

intermediate:
{"intermediateCode":[{"a":{"label":{"=":["s","\"us\""]}}},{"b":{"label":{"=":["s","\"ad\""]}}},{"c":{"label":{"=":["s","\"usb\""]}}},{"d":{"label":{"=":["s","\"bc\""]}}},{"e":{"label":{"=":["s","\"cde\""]}}}]}

output graph:
nodes: [
    {
        "label": "usb",
        "properties": {},
        "id": "usb"
    },
    {
        "label": "cde",
        "properties": {},
        "id": "cde"
    },
    {
        "label": "us",
        "properties": {},
        "id": "us"
    }
]
edges:
[
    {
        "label": "bc",
        "from": "usb",
        "to": "cde",
        "properties": {},
        "id": "bc"
    },
    {
        "label": "ad",
        "from": "us",
        "to": "usb",
        "properties": {},
        "id": "ad"
    }
]