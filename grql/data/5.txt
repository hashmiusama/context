input graph=
nodes: [
    {
        "id": "us",
        "label": "us",
        "properties": {}
    },
    {
        "id": "usb",
        "label": "usb",
        "properties": {}
    },
    {
        "id": "cde",
        "label": "cde",
        "properties": {}
    },
    {
        "id": "usc",
        "label": "usc",
        "properties": {
            "age": 32
        }
    },
    {
        "id": "uscd",
        "label": "uscd",
        "properties": {
            "age": 31
        }
    }
]
edges:[
    {
        "id": "ad",
        "label": "ad",
        "from": "us",
        "to": "usb",
        "properties": {}
    },
    {
        "id": "bc",
        "label": "bc",
        "from": "usb",
        "to": "cde",
        "properties": {}
    },
    {
        "id": "aga",
        "label": "aga",
        "from": "usb",
        "to": "usc",
        "properties": {
            "count": 200
        }
    },
    {
        "id": "aga2",
        "label": "aga2",
        "from": "usb",
        "to": "uscd",
        "properties": {
            "count": 200
        }
    }
]

query:
select [a]-/b/-[c] from graph where [$a="label",@a="usb"],[$b="label",@b="aga",@b="aga2",@b="ad"],[$c="label",@c="usc",@c="uscd"];

intermediate:
{"intermediateCode":[{"a":{"label":{"=":["s","\"usb\""]}}},{"b":{"label":{"=":["s","\"aga\"","\"aga2\"","\"ad\""]}}},{"c":{"label":{"=":["s","\"usc\"","\"uscd\""]}}}]}

output graph:
nodes:[
    {
        "label": "usc",
        "properties": {
            "age": 32
        },
        "id": "usc"
    },
    {
        "label": "usb",
        "properties": {},
        "id": "usb"
    },
    {
        "label": "uscd",
        "properties": {
            "age": 31
        },
        "id": "uscd"
    }
]
edges:[
    {
        "label": "aga",
        "from": "usb",
        "to": "usc",
        "properties": {
            "count": 200
        },
        "id": "aga"
    },
    {
        "label": "aga2",
        "from": "usb",
        "to": "uscd",
        "properties": {
            "count": 200
        },
        "id": "aga2"
    },
    {
        "label": "ad",
        "from": "us",
        "to": "usb",
        "properties": {},
        "id": "ad"
    }
]