Instructions:

-> System on which your compiler and runtime are built : Built on windows
-> Tools used : Antlr, InteliJ
-> Directions/instructions to install your language:
	to install the language, set JAVA_HOME,
	then run install.bat
-> Directions/instructions to build and run your language (compiler/runtime):
	to execute the language, run execute.bat
	then go to any browser, and type "localhost:8080/graphDisplay.html"
-> The graphs can be added to using the User Interface
-> The query can be put in the query text box and run. The visualizations will change as the query works.
-> The intermediate code is also shown on the web application
-> Because graphs require intensive writing and maintenance of the structure, it is most feasible to use the language on the web application only.
-> Link to the YouTube video:
-> masterFinal is our final git branch. Please check that for final code.
link: https://github.com/Mihika0409/SER502-Spring2017-Team-19-/tree/masterFinal


Note: Certain portions on graphDisplay.html and networkGraph.js were taken from vis.js visualization library website


