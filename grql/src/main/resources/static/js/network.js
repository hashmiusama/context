String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
var options = {groups: {
    company: {
        color: {background:'red'},
        shape: 'circle'
    },
    partnertype1: {
        label: "I'm a dot!",
        shape: 'circle',
        color: {background:'green'}
    },
    partnertype2: {
        label: "I'm a dot!",
        color: {background:'blue'},
        shape: 'circle'
    },
    partnertype3: {
        label: "I'm a dot!",
        color: {background:'white'},
        shape: 'circle'
    },
    partnertype4: {
        label: "I'm a dot!",
        color: {background:'yellow'},
        shape: 'circle'
    }
}};

var fileInput = document.getElementById("csv"),

    readFile = function () {
        var reader = new FileReader();
        reader.onload = function () {
            var nodes = new vis.DataSet([]);
	    var edgess = [];
            var edges = new vis.DataSet([]);
            var wordCheck = []
            resultArray = reader.result.split('\n');
            header = resultArray[0].split(',')
            console.log(header)
            for(rowIndex in resultArray){
		resultArray[rowIndex] = resultArray[rowIndex].replaceAll(" ","");
                head = ''
                tail = ''
                tailIndex = -1
                if(rowIndex > 0){
                    row = resultArray[rowIndex].split(',');
	
                    console.log(row);
                    head = row[0].trim()
                    for(word in row){
                        currentWord = row[word].trim();
			//currentWord = currentWord.replace(" ", "");
                        if(currentWord === null || currentWord === " " || currentWord === '' || currentWord === ""){}
                        else{
                            if(wordCheck.indexOf(currentWord) < 0){
                                wordCheck.push(currentWord);
                                nodes.add({id: wordCheck.indexOf(currentWord), label: currentWord, group:'company' });

                            }
                            tail = currentWord
                            tailIndex = word
                        }

                    }
                }
                if (wordCheck.indexOf(head) != wordCheck.indexOf(tail))
		{
		    var currEdge = {from: wordCheck.indexOf(head), to: wordCheck.indexOf(tail), label: header[tailIndex].replaceAll(" ","")}
		    if(edgess.indexOf(currEdge) < 0)                    
		    	edgess.push(currEdge)
		    else {
			console.log("I WAS CALLEDD!!");
		    }
		}	
            }
//            console.log(nodes);
//            console.log(edges);
	    edges.add(edgess);
            var container = document.getElementById('network');
            var data = {
                nodes: nodes,
                edges: edges
            };
            var network = new vis.Network(container, data, options);
	console.log(data);
	document.getElementById('edges').innerHTML = JSON.stringify(edges.get(), null, 4);
	document.getElementById('nodes').innerHTML = JSON.stringify(nodes.get(), null, 4);
        };
        // start reading the file. When it is done, calls the onload event defined above.
        reader.readAsBinaryString(fileInput.files[0]);

    };

fileInput.addEventListener('change', readFile);
