
// create a dataSet with groups
var names = ['Rank', 'Worth in Million $', 'Output in kilo gallons', 'StockPrice in $'];
var groups = new vis.DataSet();
groups.add({
    id: 0,
    content: names[0],
    options: {
        yAxisOrientation: 'right', // right, left
        drawPoints: true
    }});

groups.add({
    id: 1,
    content: names[1],
    options: {
        yAxisOrientation: 'right', // right, left
        drawPoints: true
    }});

groups.add({
    id: 2,
    content: names[2],
    options: {
        yAxisOrientation: 'right', // right, left
        drawPoints: true
    }
});

groups.add({
    id: 3,
    content: names[3],
    options: {
        yAxisOrientation: 'right', // right, left
        drawPoints: true
    }});

var container = document.getElementById('timeseriesgraphs');
var items = [
    {x: '2014-06-12', y: 6 , group: 0},
    {x: '2014-06-14', y: 17, group: 0},
    {x: '2014-06-16', y: 30, group: 0},
    {x: '2014-06-18', y: 3 , group: 0},
    {x: '2014-06-20', y: 20, group: 0},
    {x: '2014-06-22', y: 25, group: 0},
    {x: '2014-06-12', y: 15, group: 1},
    {x: '2014-06-14', y: 35, group: 1},
    {x: '2014-06-16', y: 22, group: 1},
    {x: '2014-06-18', y: 16, group: 1},
    {x: '2014-06-20', y: 23, group: 1},
    {x: '2014-06-22', y: 11, group: 1},
    {x: '2014-06-12', y: 12 , group: 2},
    {x: '2014-06-14', y: 30, group: 2},
    {x: '2014-06-16', y: 10, group: 2},
    {x: '2014-06-18', y: 53 , group: 2},
    {x: '2014-06-20', y: 30, group: 2},
    {x: '2014-06-22', y: 10, group: 2},
    {x: '2014-06-12', y: 15, group: 3},
    {x: '2014-06-13', y: 30, group: 3},
    {x: '2014-06-16', y: 10, group: 3},
    {x: '2014-06-18', y: 15, group: 3},
    {x: '2014-06-20', y: 52, group: 3},
    {x: '2014-06-22', y: 10, group: 3}
];

var dataset = new vis.DataSet(items);
var options = {
    dataAxis: {showMinorLabels: false},
    legend: {left:{position:"bottom-left"}},
    start: '2014-06-11',
    end: '2014-06-23'
};
var graph2d = new vis.Graph2d(container, dataset, groups, options);