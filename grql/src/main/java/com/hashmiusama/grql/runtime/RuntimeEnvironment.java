package com.hashmiusama.grql.runtime;

import com.hashmiusama.grql.generics.Generics;
import com.hashmiusama.grql.graph.Edge;
import com.hashmiusama.grql.graph.Graph;
import com.hashmiusama.grql.graph.Node;
import com.hashmiusama.grql.intermediator.GrQLCompiler;
import com.hashmiusama.grql.intermediator.Gri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuntimeEnvironment {
    public Graph graph;
    public Gri gri;

    public RuntimeEnvironment(){}
    public RuntimeEnvironment(Graph graph, Gri gri){
        this.graph = graph;
        this.gri = gri;
    }

    public Graph runtimeExecute(){
        ArrayList<Graph> output = new ArrayList<>();
        boolean onNode = false;
        if(gri.intermediateCode.size() == 0)
            return listToGraph(output);
        for(HashMap<String, HashMap<String, HashMap<String, ArrayList<Object>>>> variables: gri.intermediateCode){
            onNode = !onNode;
            Graph currentGraph = new Graph();
            for(Map.Entry<String, HashMap<String, HashMap<String, ArrayList<Object>>>> pair: variables.entrySet()){
//                System.out.println("variable: "+pair.getKey());
//                System.out.println("\t"+pair.getValue());
                for(Map.Entry<String, HashMap<String, ArrayList<Object>>> operator: pair.getValue().entrySet()){
                    System.out.println("\t"+"key: "+operator.getKey());
//                    System.out.println("\t\t"+operator.getValue());
                    for(Map.Entry<String, ArrayList<Object>> operands: operator.getValue().entrySet()){
                        System.out.println("\t\t"+"operator: "+operands.getKey());
                        System.out.println("\t\t\t"+"operands: "+operands.getValue());
                        currentGraph = evaluate(operator, operands, currentGraph, onNode);
                        //System.out.println(currentGraph);
                    }
                }
            }
            output.add(currentGraph);
        }
        return listToGraph(output);
    }


    public Graph evaluate(Map.Entry<String, HashMap<String, ArrayList<Object>>> operator, Map.Entry<String, ArrayList<Object>> operands, Graph currentGraph, boolean onNode){
        for(int i = 1; i < operands.getValue().size(); i++){
            String dataType = (String)operands.getValue().get(0);
            switch(dataType){
                case "s": {
                    System.out.println("String case");
                    currentGraph = stringCase(operator, operands, currentGraph, onNode, i);
                    break;
                }
                case "i":
                case "d":{
                    System.out.println("Double int case");
                    currentGraph = longDoubleCase(operator, operands, currentGraph, onNode, i);
                    break;
                }
            }
        }
        return currentGraph;
    }

    // key: operator.getKey();
    // value: operands.getValue();
    // operator: operands.getKey();
    public Graph stringCase(Map.Entry<String, HashMap<String, ArrayList<Object>>> operator, Map.Entry<String, ArrayList<Object>> operands, Graph currentGraph, boolean onNode, int i){
        if(operands.getKey().equals("=")){
            System.out.println("String equality case");
            if(onNode){
                Map<String, Node> result = graph.getNodesByPropertyValue(operator.getKey(), ((String) operands.getValue().get(i)).replace("\"",""));
                System.out.println(result);
                currentGraph.nodes.putAll(result);
            }else{
                Map<String, Edge> result = graph.getEdgesByPropertyValue(operator.getKey(), ((String) operands.getValue().get(i)).replace("\"",""));
                System.out.println(result);
                currentGraph.edges.putAll(result);
//                currentGraph.edges.putAll(graph.getEdgesByPropertyValue(operator.getKey(), (String)operands.getValue().get(i)));
            }
        }else{
            System.out.println("String inequality case");
            Map<String, Object> prop = new HashMap<>();
            prop.put(operator.getKey(), (String)operands.getValue().get(i));
            if(onNode){
                currentGraph.nodes.putAll(graph.findNodeWithoutProperties(prop));
            }else{
                currentGraph.edges.putAll(graph.findEdgeWithoutProperties(prop));
            }
        }
        return currentGraph;
    }

    public Graph longDoubleCase(Map.Entry<String, HashMap<String, ArrayList<Object>>> operator, Map.Entry<String, ArrayList<Object>> operands, Graph currentGraph, boolean onNode, int i){
        if(operator.getKey().equals("=")){
            System.out.println("long double equality");
            return equality(operator, operands, currentGraph, onNode, i);
        }else{
            System.out.println("long double inequality");
            return inequality(operator, operands, currentGraph, onNode, i);
        }
    }

    public Graph equality(Map.Entry<String, HashMap<String, ArrayList<Object>>> operator, Map.Entry<String, ArrayList<Object>> operands, Graph currentGraph, boolean onNode, int i){
        System.out.println("LD Equality Case!");

        if(onNode){
            currentGraph.nodes.putAll(graph.getNodesByPropertyValue(operator.getKey(), (String)operands.getValue().get(i)));
        }else{
            currentGraph.edges.putAll(graph.getEdgesByPropertyValue(operator.getKey(), (String)operands.getValue().get(i)));
        }
        return currentGraph;
    }


    public Graph inequality(Map.Entry<String, HashMap<String, ArrayList<Object>>> operator, Map.Entry<String, ArrayList<Object>> operands, Graph currentGraph, boolean onNode, int i){
        System.out.println("LD Inquality Case!");
        System.out.println(operands.getValue().get(i));
        System.out.println(operator.getKey());
        System.out.println(operands.getKey());
            Double d = Double.valueOf((String) operands.getValue().get(i));

/*        if(operands.getValue().get(i) instanceof String) {
            System.out.println("String found ldic");
            return currentGraph;}
            */
        if(onNode){
            System.out.println("Reached Here");

            currentGraph.nodes.putAll(graph.getNodesByInequality(operator.getKey(), d, operands.getKey()));}
        else currentGraph.edges.putAll(graph.getEdgesByInequality(operator.getKey(), d, operands.getKey()));
        return currentGraph;
    }

    public List<Graph> checkEdges(List<Graph> output){
        for(int i = 0; i < output.size(); i++){
            // node case
            if(i%2 == 0){
            }
            //edge case
            else{
                List<String> to = new ArrayList<>();
                List<String> from = new ArrayList<>();
                for(Edge edge: output.get(i).edges.values()){
                    Node nodeTo = output.get(i+1).nodes.get(edge.to);
                    if(nodeTo != null) to.add(nodeTo.label);
                    Node nodeFrom = output.get(i-1).nodes.get(edge.from);
                    if(nodeFrom != null) from.add(nodeFrom.label);
                }
                output.get(i-1).nodes.keySet().retainAll(from);
                output.get(i+1).nodes.keySet().retainAll(to);
            }
        }
        return output;
    }
    private Graph listToGraph(List<Graph> input){
        Graph output = new Graph();
        for(Graph graph: input){
            output.nodes.putAll(graph.nodes);
            output.edges.putAll(graph.edges);
        }
        return output;
    }
/*
    public static void main(String[] args){
        GrQLCompiler compiler = new GrQLCompiler();
        Graph graph = new Graph();
        Map<String, Object> prop1 = new HashMap<>();
        prop1.put("int",5);
        prop1.put("long",10);
        Map<String, Object> prop2 = new HashMap<>();
        prop2.put("string","ance");
        prop2.put("","");
        Map<String, Object> prop3 = new HashMap<>();
        prop3.put("float",1.0);
        prop3.put("double",2.00);
        Map<String, Object> prop4 = new HashMap<>();
        prop4.put("string","4");
        prop4.put("int",1232);
        Map<String, Object> prop5 = new HashMap<>();
        prop5.put("int",32);
        prop5.put("str","asdas");

        graph.addNode("usama", prop1);
        graph.addNode("bhagya", prop2);
        graph.addNode("vaishnavi", prop3);
        graph.addNode("mihika", prop4);
        graph.addNode("adnan", prop5);

        graph.addEdge("friend", "usama", "mihika", null);
        graph.addEdge("friend", "usama", "bhagya", null);
        graph.addEdge("friend", "usama", "vaishnavi", null);
        graph.addEdge("friend", "usama", "adnan", null);
        graph.addEdge("friend", "vaishnavi", "mihika", null);
        graph.addEdge("acquaintance", "adnan", "mihika", null);

//        System.out.println(Generics.fromGraphToJson(graph));
        RuntimeEnvironment rtE = new RuntimeEnvironment(graph, compiler.compile("select [a] from graph where [$a=\"int\",@a<20,@a>100];"));
        System.out.println(Generics.fromGriToJson(rtE.gri));
        System.out.println("Output: " + rtE.runtimeExecute());
    }*/
}
