package com.hashmiusama.grql.intermediator;


import com.google.gson.Gson;
import com.hashmiusama.grql.grammar.*;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by hashmiusama on 4/28/2017.
 */

public class GrQLCompiler extends GrQLBaseListener{

    String vt = null;
    String operator;
    String dataType;
    boolean tracker;
    ArrayList<HashMap<String, HashMap<String, HashMap<String, ArrayList<Object>>>>> hashMaps= new ArrayList<>();

    @Override public void exitStatement(GrQLParser.StatementContext ctx) {
        Gri gri = new Gri();
        gri.intermediateCode = this.hashMaps;
        Gson gson = new Gson();
        String intermediateCode = gson.toJson(gri);
    }

    @Override public void enterEntity(GrQLParser.EntityContext ctx) {
        String entityString = ctx.getText().toString();
        String[] array = entityString.split("-");
        for(int i=0; i<array.length; i++) {
            String var = array[i].substring(1,array[i].length()-1);
            ArrayList<HashMap<String, HashMap<String, ArrayList<HashMap<String, ArrayList<Object>>>>>> entityHash = new ArrayList<>();
            HashMap<String, HashMap<String, HashMap<String, ArrayList<Object>>>> valueHash = new HashMap<>();
            HashMap<String, HashMap<String, ArrayList<Object>>> objHash = new HashMap<>();
            populateHasMaps(entityHash, valueHash, objHash, var);
        }
    }

    public void populateHasMaps(ArrayList<HashMap<String, HashMap<String, ArrayList<HashMap<String, ArrayList<Object>>>>>> entityHash, HashMap<String, HashMap<String, HashMap<String, ArrayList<Object>>>> valueHash, HashMap<String, HashMap<String, ArrayList<Object>>> objHash, String var){
        String[] temp = var.split(",");
        for (int j = 0; j < temp.length; j++) {
            valueHash.put(temp[j], objHash);
        }
        hashMaps.add(valueHash);;
    }

    @Override public void enterCondition(GrQLParser.ConditionContext ctx) {
        String conditionString = ctx.getText().toString();
        String[] array = conditionString.split("],");
        HashMap<String,HashMap<String,HashMap<String, ArrayList<Object>>>> hmap;
        HashMap<String,HashMap<String, ArrayList<Object>>> vMap = new HashMap<>();
        HashMap<String, ArrayList<Object>> hmapList = new HashMap<>();
        for(int i=0; i<array.length; i++) {
            String var = array[i].substring(1,array[i].length()-1);
            if (var.startsWith("$")){
                int index = var.indexOf("=");
                for(int n=0; n<hashMaps.size(); n++) {
                    hmap = hashMaps.get(n);
                    String key_hash = var.substring(1,index);
                    if(hmap.containsKey(key_hash)) {
                        String[] val = var.split(",");
                        iterateThrough(val,vMap, hmapList, hmap, key_hash);
                    }
                }

            }
        }
    }

    public void iterateThrough(String[] val, HashMap<String,HashMap<String, ArrayList<Object>>> vMap, HashMap<String, ArrayList<Object>> hmapList,
                               HashMap<String,HashMap<String,HashMap<String, ArrayList<Object>>>> hmap, String key_hash){
        ArrayList<Object> valueArray = new ArrayList<>();
        int count = val.length-1;
        for(int j=0; j<val.length; j++) {
            if(j==0) {
                String temp[] = val[j].split("=");
                vt = temp[1].substring(1, temp[1].length() - 1);
                vMap.put(vt, hmapList);
                hmap.put(key_hash,vMap);
            }
            else if(val[j].startsWith("@")){
                operator = checkOperator(val[j]);
                String tempData[] = val[j].split(operator);
                if(hmapList.containsKey(operator)) {
                    valueArray.add(tempData[1]);
                    hmapList.put(operator,valueArray);
                    vMap.put(vt,hmapList);

                }
                else {
                    ArrayList<Object> valArray = new ArrayList<>();
                    dataType = checkDataType(tempData[1]);
                    valArray.add(dataType);
                    if (dataType=="s") {
                        valueArray.add(dataType);
                        valueArray.add(tempData[1]);
                        hmapList.put(operator, valueArray);
                    }
                    else {
                        valArray.add(tempData[1]);
                        hmapList.put(operator, valArray);
                    }
                    vMap.put(vt, hmapList);
                }
            }
        }
        //System.out.println(hashMaps);
    }

    public String checkDataType(String value) {
        if (value.charAt(0) == '"') {
            dataType = "s";
        }
        else {
            Boolean check = isLong(value);
            if(check) {
                dataType = "d";
            }
            else {
                dataType = "i";
            }
        }
        return dataType;
    }

    public boolean isLong(String value) {
        Pattern p = Pattern.compile("[0-9]*\\.[0-9]+");
        Matcher m = p.matcher(value);
        while (m.find()) {
            tracker = true;
        }
        return tracker;
    }

    public String checkOperator(String value) {
        if (value.indexOf("<")>0) {
            int tr = value.indexOf("<");
            if (value.charAt(tr + 1) == '=') {
                operator = "<=";
            } else {
                operator = "<";
            }
        }
        else if (value.indexOf(">")>0) {
            int tr = value.indexOf(">");
            if (value.charAt(tr + 1) == '=') {
                operator = ">=";
            } else {
                operator = ">";
            }
        }
        else if (value.indexOf("!")>0) {
            int tr = value.indexOf("!");
            if(value.charAt(tr+1)=='='){
                operator = "!=";
            }
        }
        else if (value.indexOf("=")>0) {
            operator = "=";
        }
        return  operator;
    }

    public Gri compile(String query){
        GrQLLexer lexer = null;
        lexer = new GrQLLexer(new ANTLRInputStream(query));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        GrQLParser parser = new GrQLParser(tokens);
        ParseTree tree = parser.statement();
        //System.out.println(tree.toStringTree());
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, tree);
        Gri gri = new Gri();
        gri.intermediateCode = this.hashMaps;
        return gri;
    }
/*
    public static void main(String args[]){
        args = null;
        args = new String[]{"c",""};
        GrQLCompiler grQLCompiler = new GrQLCompiler();
        //Gri gri = grQLCompiler.compile("select [name,school]-/type/-[classname] from graph where [$name=\"studentname\",@name>5,@name<6],[$school=\"university\",@school=\"ASU\",@school=\"IUB\"],[$type=\"relationshiptype\",@type=\"hasclass\",@type=\"hasCourse\"],[$classname=\"classroom\",@classname<=8.9,@classname>=9.2];");
        ////System.out.println(gri.intermediateCode);
        while(true){
            Scanner scanner = new Scanner(System.in);
            String query = scanner.nextLine();
            args[1] = query;
            if(query.equals("")){
                break;
            }
            GrQLLexer lexer = null;
            if(args[0].equals("f")){
                try {
                    lexer = new GrQLLexer(new ANTLRFileStream(args[1]));
                } catch (IOException e) {
                    //System.out.println("File not read: " + e);
                }
            }else if (args[0].equals("c")){
                lexer = new GrQLLexer(new ANTLRInputStream(args[1]));
            }
            if(lexer == null){
                //System.out.println("Code not found");
            }else {

                ////System.out.println("Reached here.");
                CommonTokenStream tokens = new CommonTokenStream(lexer);
                GrQLParser parser = new GrQLParser(tokens);
                ParseTree tree = parser.statement();
                //System.out.println(tree.toStringTree());
                ParseTreeWalker walker = new ParseTreeWalker();
                walker.walk(new GrQLCompiler(), tree);

            }
        }

    }*/
}
