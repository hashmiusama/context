package com.hashmiusama.grql.intermediator;

import java.util.ArrayList;
import java.util.HashMap;

public class Gri {
    public ArrayList<HashMap<String, HashMap<String, HashMap<String, ArrayList<Object>>>>> intermediateCode= new ArrayList<>();
    public Gri(){
    }
    public Gri(ArrayList<HashMap<String, HashMap<String, HashMap<String, ArrayList<Object>>>>> intermediateMap){
        this.intermediateCode = intermediateMap;
    }

    @Override
    public String toString() {
        return intermediateCode.toString();
    }
}
