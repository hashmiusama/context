package com.hashmiusama.grql.generics;

import com.google.gson.Gson;
import com.hashmiusama.grql.graph.Graph;
import com.hashmiusama.grql.intermediator.Gri;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Generics {
    private static Gson gson = new Gson();
    private static ArrayList<Class> numeric = new ArrayList<>();
    static{
        numeric.add(int.class);
        numeric.add(float.class);
        numeric.add(double.class);
        numeric.add(long.class);
    }
    public static void writeGraph(String fileName, Graph graph) throws IOException {
        String json = gson.toJson(graph);
        try (Writer writer = new FileWriter(fileName)) {
            gson.toJson(graph, writer);
        }
    }
    public static Graph readGraph(String fileName) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        return gson.fromJson(br, Graph.class);
    }
    public static String fromGraphToJson(Graph graph){
        return gson.toJson(graph);
    }
    public static Graph fromJsonToGraph(String json){
        return gson.fromJson(json, Graph.class);
    }
    public static String fromGriToJson(Gri gri){
        return gson.toJson(gri);
    }
    public static String fromJsonToGri(String json){
        return gson.toJson(json, Gri.class);
    }

    public static Object treatProperty(Object property){
        if(property == null) return null;
        if(property instanceof Map) {
            Map<String, Object> map = (HashMap)property;
            map = (Map<String, Object>) doubler(map);
            return map;
        }else if (property instanceof List){
            List<Object> list = (ArrayList)property;
            list = (ArrayList<Object>) doubler(list);
            return list;
        } else property = doubler(property);
//        System.out.println("\tafter: "+property.getClass());
//        System.out.println(property);
        return property;
    }
    public static Object doubler(Map<String, Object> properties){
//        System.out.println("Map Doubler Called");
        if(properties == null) return null;
        for(String key: properties.keySet()){
            properties.put(key, doubler(properties.get(key)));
        }
//        System.out.println(properties);
        return properties;
    }
    public static Object doubler(List<Object> properties){
        if(properties == null) return null;
        for(int i = 0; i < properties.size(); i++){
            properties.set(i, treatProperty(properties.get(i)));
        }
//        System.out.println(properties);
        return properties;
    }
    public static Object doubler(Object value){
        if(value == null) return null;
        if(value instanceof Integer || value instanceof Long){
//            System.out.println("IntegerLong Doubler Called");
            value = (Double)(double)((int) value);
//            System.out.println(value);
            return value;
        }else
            if(value instanceof Float || value instanceof Double) {
//                System.out.println("FloatDouble Doubler Called");
                value = (Double)((double)value);
//                System.out.println(value);
                return value;
        }
        return value;
    }
}
