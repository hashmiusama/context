package com.hashmiusama.grql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrqlApplication {
	public static void main(String[] args) {
		SpringApplication.run(GrqlApplication.class, args);
	}
}

//select [a]-/b/-[c]-/d/-[e] from graph where [$a="label",@a="us"],[$b="label",@b="ad"],[$c="label",@c="usb"],[$d="label",@d="bc"],[$e="label",@e="cde"];
