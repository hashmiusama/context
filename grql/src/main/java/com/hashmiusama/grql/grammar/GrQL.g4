grammar GrQL;
    
statement:SELECT WS entities WS FROM WS graph WS WHERE WS conditions SEMI
	 |SELECT WS entities WS FROM WS graph SEMI
	 |SELECT WS STAR WS FROM WS graph SEMI
	 |SELECT WS STAR WS FROM WS graph WS WHERE WS conditions SEMI
	 ;


entity:node 
      |edge 
      |graph
      ;

entities:entity 
        |entity','entities
        ;

graph:node'-'edge'-'node
	 |STRING
	 |node'-'edge'-'graph
	 ;

node:'['pairs']';
edge:'/'pairs'/';
pairs:pair
     | pair','pairs
     ;
pair: STRING;

condition:'[' keyexpression ',' valueexpressions ']' ;
keyexpression: '$' key EQ STRING ;

valueexpression: '@' val operator valallowed ;

valueexpressions: valueexpression
                | valueexpression',' valueexpressions ;

key: STRING ;
val: STRING ;

conditions: condition
          | condition','conditions
          ;

valallowed:INTEGER
          |FLOAT
          |STRING
          |STAR
          ;

operator:EQ
		|LTH
		|GTH
		|NOT_EQ
		|LET
		|GET
		;

EQ:'=';

LTH:'<';

GTH:'>';

NOT_EQ:'!=';

LET:'<=';

GET:'>=';

SELECT:'select';

FROM:'from';

WHERE:'where';

SEMI:';';

NEWLINE:'\r'?'\n'->skip;

WS:( ' ' | '\t' )+;

INTEGER:[-+]?[0-9]+;

FLOAT:[-+]?[0-9]+('.' [0-9]+)?;

STRING:[A-Za-z0-9"]+ ;
STAR: '*';