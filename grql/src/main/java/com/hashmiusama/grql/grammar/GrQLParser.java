// Generated from C:/Users/hashmiusama/IdeaProjects/grql/src/main/java/com/hashmiusama/grammar\GrQL.g4 by ANTLR 4.7
package com.hashmiusama.grql.grammar;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GrQLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, EQ=8, LTH=9, GTH=10, 
		NOT_EQ=11, LET=12, GET=13, SELECT=14, FROM=15, WHERE=16, SEMI=17, NEWLINE=18, 
		WS=19, INTEGER=20, FLOAT=21, STRING=22, STAR=23;
	public static final int
		RULE_statement = 0, RULE_entity = 1, RULE_entities = 2, RULE_graph = 3, 
		RULE_node = 4, RULE_edge = 5, RULE_pairs = 6, RULE_pair = 7, RULE_condition = 8, 
		RULE_keyexpression = 9, RULE_valueexpression = 10, RULE_valueexpressions = 11, 
		RULE_key = 12, RULE_val = 13, RULE_conditions = 14, RULE_valallowed = 15, 
		RULE_operator = 16;
	public static final String[] ruleNames = {
		"statement", "entity", "entities", "graph", "node", "edge", "pairs", "pair", 
		"condition", "keyexpression", "valueexpression", "valueexpressions", "key", 
		"val", "conditions", "valallowed", "operator"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "','", "'-'", "'['", "']'", "'/'", "'$'", "'@'", "'='", "'<'", "'>'", 
		"'!='", "'<='", "'>='", "'select'", "'from'", "'where'", "';'", null, 
		null, null, null, null, "'*'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, "EQ", "LTH", "GTH", "NOT_EQ", 
		"LET", "GET", "SELECT", "FROM", "WHERE", "SEMI", "NEWLINE", "WS", "INTEGER", 
		"FLOAT", "STRING", "STAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "GrQL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GrQLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StatementContext extends ParserRuleContext {
		public TerminalNode SELECT() { return getToken(GrQLParser.SELECT, 0); }
		public List<TerminalNode> WS() { return getTokens(GrQLParser.WS); }
		public TerminalNode WS(int i) {
			return getToken(GrQLParser.WS, i);
		}
		public EntitiesContext entities() {
			return getRuleContext(EntitiesContext.class,0);
		}
		public TerminalNode FROM() { return getToken(GrQLParser.FROM, 0); }
		public GraphContext graph() {
			return getRuleContext(GraphContext.class,0);
		}
		public TerminalNode WHERE() { return getToken(GrQLParser.WHERE, 0); }
		public ConditionsContext conditions() {
			return getRuleContext(ConditionsContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(GrQLParser.SEMI, 0); }
		public TerminalNode STAR() { return getToken(GrQLParser.STAR, 0); }
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statement);
		try {
			setState(78);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(34);
				match(SELECT);
				setState(35);
				match(WS);
				setState(36);
				entities();
				setState(37);
				match(WS);
				setState(38);
				match(FROM);
				setState(39);
				match(WS);
				setState(40);
				graph();
				setState(41);
				match(WS);
				setState(42);
				match(WHERE);
				setState(43);
				match(WS);
				setState(44);
				conditions();
				setState(45);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(47);
				match(SELECT);
				setState(48);
				match(WS);
				setState(49);
				entities();
				setState(50);
				match(WS);
				setState(51);
				match(FROM);
				setState(52);
				match(WS);
				setState(53);
				graph();
				setState(54);
				match(SEMI);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(56);
				match(SELECT);
				setState(57);
				match(WS);
				setState(58);
				match(STAR);
				setState(59);
				match(WS);
				setState(60);
				match(FROM);
				setState(61);
				match(WS);
				setState(62);
				graph();
				setState(63);
				match(SEMI);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(65);
				match(SELECT);
				setState(66);
				match(WS);
				setState(67);
				match(STAR);
				setState(68);
				match(WS);
				setState(69);
				match(FROM);
				setState(70);
				match(WS);
				setState(71);
				graph();
				setState(72);
				match(WS);
				setState(73);
				match(WHERE);
				setState(74);
				match(WS);
				setState(75);
				conditions();
				setState(76);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityContext extends ParserRuleContext {
		public NodeContext node() {
			return getRuleContext(NodeContext.class,0);
		}
		public EdgeContext edge() {
			return getRuleContext(EdgeContext.class,0);
		}
		public GraphContext graph() {
			return getRuleContext(GraphContext.class,0);
		}
		public EntityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterEntity(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitEntity(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitEntity(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntityContext entity() throws RecognitionException {
		EntityContext _localctx = new EntityContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_entity);
		try {
			setState(83);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(80);
				node();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(81);
				edge();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(82);
				graph();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntitiesContext extends ParserRuleContext {
		public EntityContext entity() {
			return getRuleContext(EntityContext.class,0);
		}
		public EntitiesContext entities() {
			return getRuleContext(EntitiesContext.class,0);
		}
		public EntitiesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entities; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterEntities(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitEntities(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitEntities(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntitiesContext entities() throws RecognitionException {
		EntitiesContext _localctx = new EntitiesContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_entities);
		try {
			setState(90);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(85);
				entity();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(86);
				entity();
				setState(87);
				match(T__0);
				setState(88);
				entities();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphContext extends ParserRuleContext {
		public List<NodeContext> node() {
			return getRuleContexts(NodeContext.class);
		}
		public NodeContext node(int i) {
			return getRuleContext(NodeContext.class,i);
		}
		public EdgeContext edge() {
			return getRuleContext(EdgeContext.class,0);
		}
		public TerminalNode STRING() { return getToken(GrQLParser.STRING, 0); }
		public GraphContext graph() {
			return getRuleContext(GraphContext.class,0);
		}
		public GraphContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graph; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterGraph(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitGraph(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitGraph(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GraphContext graph() throws RecognitionException {
		GraphContext _localctx = new GraphContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_graph);
		try {
			setState(105);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(92);
				node();
				setState(93);
				match(T__1);
				setState(94);
				edge();
				setState(95);
				match(T__1);
				setState(96);
				node();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(98);
				match(STRING);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(99);
				node();
				setState(100);
				match(T__1);
				setState(101);
				edge();
				setState(102);
				match(T__1);
				setState(103);
				graph();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NodeContext extends ParserRuleContext {
		public PairsContext pairs() {
			return getRuleContext(PairsContext.class,0);
		}
		public NodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_node; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterNode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitNode(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitNode(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NodeContext node() throws RecognitionException {
		NodeContext _localctx = new NodeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_node);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(T__2);
			setState(108);
			pairs();
			setState(109);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EdgeContext extends ParserRuleContext {
		public PairsContext pairs() {
			return getRuleContext(PairsContext.class,0);
		}
		public EdgeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_edge; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterEdge(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitEdge(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitEdge(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EdgeContext edge() throws RecognitionException {
		EdgeContext _localctx = new EdgeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_edge);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(T__4);
			setState(112);
			pairs();
			setState(113);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PairsContext extends ParserRuleContext {
		public PairContext pair() {
			return getRuleContext(PairContext.class,0);
		}
		public PairsContext pairs() {
			return getRuleContext(PairsContext.class,0);
		}
		public PairsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pairs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterPairs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitPairs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitPairs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PairsContext pairs() throws RecognitionException {
		PairsContext _localctx = new PairsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_pairs);
		try {
			setState(120);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(115);
				pair();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(116);
				pair();
				setState(117);
				match(T__0);
				setState(118);
				pairs();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PairContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(GrQLParser.STRING, 0); }
		public PairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pair; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterPair(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitPair(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitPair(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PairContext pair() throws RecognitionException {
		PairContext _localctx = new PairContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_pair);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public KeyexpressionContext keyexpression() {
			return getRuleContext(KeyexpressionContext.class,0);
		}
		public ValueexpressionsContext valueexpressions() {
			return getRuleContext(ValueexpressionsContext.class,0);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			match(T__2);
			setState(125);
			keyexpression();
			setState(126);
			match(T__0);
			setState(127);
			valueexpressions();
			setState(128);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyexpressionContext extends ParserRuleContext {
		public KeyContext key() {
			return getRuleContext(KeyContext.class,0);
		}
		public TerminalNode EQ() { return getToken(GrQLParser.EQ, 0); }
		public TerminalNode STRING() { return getToken(GrQLParser.STRING, 0); }
		public KeyexpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyexpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterKeyexpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitKeyexpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitKeyexpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeyexpressionContext keyexpression() throws RecognitionException {
		KeyexpressionContext _localctx = new KeyexpressionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_keyexpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130);
			match(T__5);
			setState(131);
			key();
			setState(132);
			match(EQ);
			setState(133);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueexpressionContext extends ParserRuleContext {
		public ValContext val() {
			return getRuleContext(ValContext.class,0);
		}
		public OperatorContext operator() {
			return getRuleContext(OperatorContext.class,0);
		}
		public ValallowedContext valallowed() {
			return getRuleContext(ValallowedContext.class,0);
		}
		public ValueexpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueexpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterValueexpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitValueexpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitValueexpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueexpressionContext valueexpression() throws RecognitionException {
		ValueexpressionContext _localctx = new ValueexpressionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_valueexpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(135);
			match(T__6);
			setState(136);
			val();
			setState(137);
			operator();
			setState(138);
			valallowed();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueexpressionsContext extends ParserRuleContext {
		public ValueexpressionContext valueexpression() {
			return getRuleContext(ValueexpressionContext.class,0);
		}
		public ValueexpressionsContext valueexpressions() {
			return getRuleContext(ValueexpressionsContext.class,0);
		}
		public ValueexpressionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueexpressions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterValueexpressions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitValueexpressions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitValueexpressions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueexpressionsContext valueexpressions() throws RecognitionException {
		ValueexpressionsContext _localctx = new ValueexpressionsContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_valueexpressions);
		try {
			setState(145);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(140);
				valueexpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(141);
				valueexpression();
				setState(142);
				match(T__0);
				setState(143);
				valueexpressions();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(GrQLParser.STRING, 0); }
		public KeyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterKey(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitKey(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitKey(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeyContext key() throws RecognitionException {
		KeyContext _localctx = new KeyContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_key);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(147);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(GrQLParser.STRING, 0); }
		public ValContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_val; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterVal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitVal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitVal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValContext val() throws RecognitionException {
		ValContext _localctx = new ValContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_val);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionsContext extends ParserRuleContext {
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public ConditionsContext conditions() {
			return getRuleContext(ConditionsContext.class,0);
		}
		public ConditionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterConditions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitConditions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitConditions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionsContext conditions() throws RecognitionException {
		ConditionsContext _localctx = new ConditionsContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_conditions);
		try {
			setState(156);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(151);
				condition();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(152);
				condition();
				setState(153);
				match(T__0);
				setState(154);
				conditions();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValallowedContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(GrQLParser.INTEGER, 0); }
		public TerminalNode FLOAT() { return getToken(GrQLParser.FLOAT, 0); }
		public TerminalNode STRING() { return getToken(GrQLParser.STRING, 0); }
		public TerminalNode STAR() { return getToken(GrQLParser.STAR, 0); }
		public ValallowedContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valallowed; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterValallowed(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitValallowed(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitValallowed(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValallowedContext valallowed() throws RecognitionException {
		ValallowedContext _localctx = new ValallowedContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_valallowed);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(158);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTEGER) | (1L << FLOAT) | (1L << STRING) | (1L << STAR))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperatorContext extends ParserRuleContext {
		public TerminalNode EQ() { return getToken(GrQLParser.EQ, 0); }
		public TerminalNode LTH() { return getToken(GrQLParser.LTH, 0); }
		public TerminalNode GTH() { return getToken(GrQLParser.GTH, 0); }
		public TerminalNode NOT_EQ() { return getToken(GrQLParser.NOT_EQ, 0); }
		public TerminalNode LET() { return getToken(GrQLParser.LET, 0); }
		public TerminalNode GET() { return getToken(GrQLParser.GET, 0); }
		public OperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).enterOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrQLListener ) ((GrQLListener)listener).exitOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrQLVisitor ) return ((GrQLVisitor<? extends T>)visitor).visitOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperatorContext operator() throws RecognitionException {
		OperatorContext _localctx = new OperatorContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << LTH) | (1L << GTH) | (1L << NOT_EQ) | (1L << LET) | (1L << GET))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\31\u00a5\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2Q\n\2\3\3\3\3\3\3\5\3V\n\3\3\4"+
		"\3\4\3\4\3\4\3\4\5\4]\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\5\5l\n\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b"+
		"\5\b{\n\b\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\f"+
		"\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\5\r\u0094\n\r\3\16\3\16\3\17\3\17"+
		"\3\20\3\20\3\20\3\20\3\20\5\20\u009f\n\20\3\21\3\21\3\22\3\22\3\22\2\2"+
		"\23\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"\2\4\3\2\26\31\3\2\n\17\2"+
		"\u009e\2P\3\2\2\2\4U\3\2\2\2\6\\\3\2\2\2\bk\3\2\2\2\nm\3\2\2\2\fq\3\2"+
		"\2\2\16z\3\2\2\2\20|\3\2\2\2\22~\3\2\2\2\24\u0084\3\2\2\2\26\u0089\3\2"+
		"\2\2\30\u0093\3\2\2\2\32\u0095\3\2\2\2\34\u0097\3\2\2\2\36\u009e\3\2\2"+
		"\2 \u00a0\3\2\2\2\"\u00a2\3\2\2\2$%\7\20\2\2%&\7\25\2\2&\'\5\6\4\2\'("+
		"\7\25\2\2()\7\21\2\2)*\7\25\2\2*+\5\b\5\2+,\7\25\2\2,-\7\22\2\2-.\7\25"+
		"\2\2./\5\36\20\2/\60\7\23\2\2\60Q\3\2\2\2\61\62\7\20\2\2\62\63\7\25\2"+
		"\2\63\64\5\6\4\2\64\65\7\25\2\2\65\66\7\21\2\2\66\67\7\25\2\2\678\5\b"+
		"\5\289\7\23\2\29Q\3\2\2\2:;\7\20\2\2;<\7\25\2\2<=\7\31\2\2=>\7\25\2\2"+
		">?\7\21\2\2?@\7\25\2\2@A\5\b\5\2AB\7\23\2\2BQ\3\2\2\2CD\7\20\2\2DE\7\25"+
		"\2\2EF\7\31\2\2FG\7\25\2\2GH\7\21\2\2HI\7\25\2\2IJ\5\b\5\2JK\7\25\2\2"+
		"KL\7\22\2\2LM\7\25\2\2MN\5\36\20\2NO\7\23\2\2OQ\3\2\2\2P$\3\2\2\2P\61"+
		"\3\2\2\2P:\3\2\2\2PC\3\2\2\2Q\3\3\2\2\2RV\5\n\6\2SV\5\f\7\2TV\5\b\5\2"+
		"UR\3\2\2\2US\3\2\2\2UT\3\2\2\2V\5\3\2\2\2W]\5\4\3\2XY\5\4\3\2YZ\7\3\2"+
		"\2Z[\5\6\4\2[]\3\2\2\2\\W\3\2\2\2\\X\3\2\2\2]\7\3\2\2\2^_\5\n\6\2_`\7"+
		"\4\2\2`a\5\f\7\2ab\7\4\2\2bc\5\n\6\2cl\3\2\2\2dl\7\30\2\2ef\5\n\6\2fg"+
		"\7\4\2\2gh\5\f\7\2hi\7\4\2\2ij\5\b\5\2jl\3\2\2\2k^\3\2\2\2kd\3\2\2\2k"+
		"e\3\2\2\2l\t\3\2\2\2mn\7\5\2\2no\5\16\b\2op\7\6\2\2p\13\3\2\2\2qr\7\7"+
		"\2\2rs\5\16\b\2st\7\7\2\2t\r\3\2\2\2u{\5\20\t\2vw\5\20\t\2wx\7\3\2\2x"+
		"y\5\16\b\2y{\3\2\2\2zu\3\2\2\2zv\3\2\2\2{\17\3\2\2\2|}\7\30\2\2}\21\3"+
		"\2\2\2~\177\7\5\2\2\177\u0080\5\24\13\2\u0080\u0081\7\3\2\2\u0081\u0082"+
		"\5\30\r\2\u0082\u0083\7\6\2\2\u0083\23\3\2\2\2\u0084\u0085\7\b\2\2\u0085"+
		"\u0086\5\32\16\2\u0086\u0087\7\n\2\2\u0087\u0088\7\30\2\2\u0088\25\3\2"+
		"\2\2\u0089\u008a\7\t\2\2\u008a\u008b\5\34\17\2\u008b\u008c\5\"\22\2\u008c"+
		"\u008d\5 \21\2\u008d\27\3\2\2\2\u008e\u0094\5\26\f\2\u008f\u0090\5\26"+
		"\f\2\u0090\u0091\7\3\2\2\u0091\u0092\5\30\r\2\u0092\u0094\3\2\2\2\u0093"+
		"\u008e\3\2\2\2\u0093\u008f\3\2\2\2\u0094\31\3\2\2\2\u0095\u0096\7\30\2"+
		"\2\u0096\33\3\2\2\2\u0097\u0098\7\30\2\2\u0098\35\3\2\2\2\u0099\u009f"+
		"\5\22\n\2\u009a\u009b\5\22\n\2\u009b\u009c\7\3\2\2\u009c\u009d\5\36\20"+
		"\2\u009d\u009f\3\2\2\2\u009e\u0099\3\2\2\2\u009e\u009a\3\2\2\2\u009f\37"+
		"\3\2\2\2\u00a0\u00a1\t\2\2\2\u00a1!\3\2\2\2\u00a2\u00a3\t\3\2\2\u00a3"+
		"#\3\2\2\2\tPU\\kz\u0093\u009e";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
