// Generated from C:/Users/hashmiusama/IdeaProjects/grql/src/main/java/com/hashmiusama/grammar\GrQL.g4 by ANTLR 4.7
package com.hashmiusama.grql.grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrQLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrQLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrQLParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(GrQLParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#entity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEntity(GrQLParser.EntityContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#entities}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEntities(GrQLParser.EntitiesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#graph}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGraph(GrQLParser.GraphContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#node}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNode(GrQLParser.NodeContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#edge}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEdge(GrQLParser.EdgeContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#pairs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPairs(GrQLParser.PairsContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#pair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair(GrQLParser.PairContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(GrQLParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#keyexpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyexpression(GrQLParser.KeyexpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#valueexpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValueexpression(GrQLParser.ValueexpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#valueexpressions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValueexpressions(GrQLParser.ValueexpressionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKey(GrQLParser.KeyContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVal(GrQLParser.ValContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#conditions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditions(GrQLParser.ConditionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#valallowed}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValallowed(GrQLParser.ValallowedContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrQLParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator(GrQLParser.OperatorContext ctx);
}
