package com.hashmiusama.grql.graph;

import com.hashmiusama.grql.generics.Generics;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;

public class Node implements Serializable{
    public String label = "";
    public Map<String, Object> properties = new HashMap<>();
    Node(){
    }
    Node(String label, Map<String, Object> properties){
        this.label = label;
        properties = (HashMap<String, Object>) Generics.doubler(properties);
//        checkProperties(properties);
        this.properties.putAll(properties);

//        checkProperties();
    }
/*    public void checkProperties(){
//        System.out.println("\n\nChecking properties");
        for(Object o: properties.values()){
//            System.out.println(o.getClass());
        }
//        System.out.println("\n\n");
    }

    public void checkProperties(Map<String, Object> properties){
        System.out.println("\n\nChecking properties");
        for(Object o: properties.values()){
            System.out.println(o.getClass());
        }
        System.out.println("\n\n");
    }
        */
    public void addProperty(String key, Object value){
        value = Generics.doubler(value);
        properties.put(key, value);
        //checkProperties();
    }
    public boolean hasProperty(String property){
        return properties.get(property) != null;
    }
    public Object propertyValue(String key){
        return properties.get(key);
    }
    public Set<String> getPropertyNames(){
        return properties.keySet();
    }
    public Set<Map.Entry<String, Object>> getProperties(){
        return properties.entrySet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;

        Node node = (Node) o;

        return label.equals(node.label);
    }

    @Override
    public int hashCode() {
        return label.hashCode();
    }

    @Override
    public String toString() {
        return "{" +
                "\"label\":'" + label + '\'' +
                ",\"properties\":" + properties +
                '}';
    }
}
