package com.hashmiusama.grql.graph;

import com.hashmiusama.grql.generics.Generics;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Edge implements Serializable{
    public String label;
    public String from;
    public String to;
    Map<String, Object> properties;

    Edge(String label, String from, String to, Map<String, Object> properties){
        this.label = label;
        this.from = from;
        this.to = to;
        properties = (HashMap<String, Object>) Generics.doubler(properties);
        this.properties = properties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;

        Edge edge = (Edge) o;

        if (!label.equals(edge.label)) return false;
        if (!from.equals(edge.from)) return false;
        return to.equals(edge.to);
    }

    @Override
    public int hashCode() {
        int result = label.hashCode();
        result = 31 * result + from.hashCode();
        result = 31 * result + to.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "\"label\":\"" + label + "\"," +
                "\"from\":\"" + from + "\"," +
                "\"to\":\"" + to + "\","+
                "\"properties\":" + properties +
                '}';
    }
}
